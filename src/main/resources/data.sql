INSERT INTO `user` (username, password) VALUES ('admin', '$2a$10$iz1QiuSVUMFcXdx5Xs2mr.OdTZBuutK0GrKqWS8x2OhaDz8jKV5ei');

INSERT INTO `education_type` (`id`, `name`) VALUES
	(5, 'Alg'),
	(2, 'Kesk'),
	(3, 'Kõrg - akadeemiline'),
	(4, 'Kõrg - rakenduslik'),
	(1, 'Põhi');

INSERT INTO `person` (`id`, `first_name`, `last_name`, `dob`, `education_type_id`) VALUES
	(1, 'Mati', 'Maasikas', '1970-06-19', 4),
	(2, 'Kairit', 'Kaarik', '1992-05-05', 3),
	(3, 'Rainer', 'Muhk', '1968-03-19', NULL);

INSERT INTO `education` (`id`, `school`, `graduation_year`, `person_id`) VALUES
	(1, 'Viljandi 87 Keskkool', 1987, 1),
	(2, 'Huugametsa Raketikool', 1992, 1),
	(3, 'Pärnu Ärigümnaasium', 2002, 2),
	(4, 'Tartu Ülikool', 2005, 2),
	(5, 'Narva Kolledž', 2010, 2);
