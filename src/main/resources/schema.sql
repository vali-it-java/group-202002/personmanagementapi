DROP TABLE IF EXISTS `education`;
DROP TABLE IF EXISTS `person`;
DROP TABLE IF EXISTS `education_type`;
DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `username` VARCHAR(190) NOT NULL,
    `password` VARCHAR(190) NOT NULL,
    PRIMARY KEY (id),
    UNIQUE(username)
);

CREATE TABLE IF NOT EXISTS `education_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Index 2` (`name`)
);

CREATE TABLE IF NOT EXISTS `person` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50),
  `last_name` varchar(50),
  `dob` date DEFAULT NULL,
  `education_type_id` int(11) DEFAULT NULL,
  `profile_photo` varchar(250) NULL,
  PRIMARY KEY (`id`),
  KEY `FK_person_education_type` (`education_type_id`),
  CONSTRAINT `FK_person_education_type` FOREIGN KEY (`education_type_id`) REFERENCES `education_type` (`id`)
);

CREATE TABLE IF NOT EXISTS `education` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `school` varchar(100) NOT NULL,
  `graduation_year` int(11) NOT NULL,
  `person_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_education_person` (`person_id`),
  CONSTRAINT `FK_education_person` FOREIGN KEY (`person_id`) REFERENCES `person` (`id`)
);