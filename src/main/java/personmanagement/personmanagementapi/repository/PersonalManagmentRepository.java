package personmanagement.personmanagementapi.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import personmanagement.personmanagementapi.model.Education;
import personmanagement.personmanagementapi.model.EducationType;
import personmanagement.personmanagementapi.model.Person;

import java.util.List;

@Repository
public class PersonalManagmentRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public List<Person> fetchPersons() {
        return jdbcTemplate.query("select * from person",
                (row, number) -> {
                    return new Person(
                            row.getInt("id"),
                            row.getString("first_name"),
                            row.getString("last_name"),
                            row.getDate("dob").toLocalDate(),
                            this.fetchEducations(row.getInt("id")),
                            row.getInt("education_type_id"),
                            row.getString("profile_photo")
                    );
                });
    }

    public List<Education> fetchEducations(int personId) {
        return jdbcTemplate.query("select * from education where person_id = ?",
                new Object[]{ personId },
                (row, number) -> {
                    return new Education(
                            row.getInt("id"),
                            row.getString("school"),
                            row.getInt("graduation_year"),
                            row.getInt("person_id")
                    );
                });
    }

    public void addPerson(Person person) {
        jdbcTemplate.update("insert into person (first_name, last_name, dob, education_type_id, profile_photo) values(?, ?, ?, ?, ?)",
                person.getFirstName(), person.getLastName(), person.getDateOfBirth(), person.getEducationTypeId(), person.getProfilePhotoUrl());
    }

    public void updatePerson(Person person) {
        if (person.getProfilePhotoUrl() != null) {
            jdbcTemplate.update("update person set first_name = ?, last_name = ?, dob = ?, education_type_id = ?, profile_photo = ? where id = ?",
                    person.getFirstName(), person.getLastName(), person.getDateOfBirth(), person.getEducationTypeId(), person.getProfilePhotoUrl(), person.getId());
        } else {
            jdbcTemplate.update("update person set first_name = ?, last_name = ?, dob = ?, education_type_id = ? where id = ?",
                    person.getFirstName(), person.getLastName(), person.getDateOfBirth(), person.getEducationTypeId(), person.getId());
        }
    }

    public void addEducation(Education education) {
        jdbcTemplate.update("insert into education (school, graduation_year, person_id) value (?, ?, ?)",
                education.getSchool(), education.getGraduationYear(), education.getPersonId());
    }

    public void updateEducation(Education education) {
        jdbcTemplate.update("update education set school = ?, graduation_year = ?, person_id = ? where id = ?",
            education.getSchool(), education.getGraduationYear(), education.getPersonId(), education.getId());
    }

    public List<EducationType> getEducationTypes() {
        return jdbcTemplate.query("select * from education_type",
                (row, number) -> {
                    return new EducationType(row.getInt("id"), row.getString("name"));
        });
    }

}
