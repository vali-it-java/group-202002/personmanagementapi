package personmanagement.personmanagementapi.service;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfWriter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.UUID;

@Service
public class FileService {

    @Value("${file.upload-dir}")
    private String uploadDir;

    private Path getUploadDir() {
        return Paths.get(this.uploadDir).toAbsolutePath().normalize();
    }

    public String storeFile(MultipartFile file) throws IOException {
        String fileName = generateFileName(file);
        Path targetLocation = this.getUploadDir().resolve(fileName);
        Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
        return fileName;
    }

    public Resource loadFileAsResource(String fileName) throws MalformedURLException {
        Path filePath = this.getUploadDir().resolve(fileName).normalize();
        return new UrlResource(filePath.toUri());
    }

    private String generateFileName(MultipartFile inputFile) {
        String fileName = StringUtils.cleanPath(inputFile.getOriginalFilename());
        String extension = "";
        int i = fileName.lastIndexOf(".");
        if (i > 0) {
            extension = fileName.substring(i);
        }
        return UUID.randomUUID().toString() + extension;
    }

    private String generatePdfFileName() {
        return UUID.randomUUID().toString() + ".pdf";
    }

    public Resource generatePdf(String message) throws FileNotFoundException, DocumentException, MalformedURLException {
        Document document = new Document();
        String pdfFileName = this.generatePdfFileName();
        String pdfFilePath = this.getUploadDir() + "/" + pdfFileName;
        PdfWriter.getInstance(document, new FileOutputStream(pdfFilePath));

        document.open();
        Font font = FontFactory.getFont(FontFactory.COURIER, 16, BaseColor.BLACK);
        Chunk chunk = new Chunk(message, font);

        document.add(chunk);
        document.close();

        return this.loadFileAsResource(pdfFileName);
    }
}
