package personmanagement.personmanagementapi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import personmanagement.personmanagementapi.model.Education;
import personmanagement.personmanagementapi.model.Person;
import personmanagement.personmanagementapi.repository.PersonalManagmentRepository;

import java.time.LocalDate;

@Service
public class PersonManagementService {

    @Autowired
    private PersonalManagmentRepository personalManagmentRepository;

    public void savePerson(Person person) {
        Assert.notNull(person, "Person not specified!");
        Assert.notNull(person.getFirstName(), "Person's first name not specified!");
        Assert.isTrue(person.getFirstName().length() >= 2, "Person's first name not long enough!");
        Assert.isTrue(person.getFirstName().length() <= 25, "Person's first name too long!");
        Assert.isTrue(person.getLastName().length() >= 2, "Person's last name not long enough!");
        Assert.isTrue(person.getLastName().length() <= 25, "Person's last name too long!");
        Assert.isTrue(person.getDateOfBirth().isBefore(LocalDate.now()), "We won't hire non-born persons!");
        Assert.isTrue(person.getEducationTypeId() > 0, "Person must have education type defined!");
        if (person.getId() > 0) {
            personalManagmentRepository.updatePerson(person);
        } else {
            personalManagmentRepository.addPerson(person);
        }
    }

    public void saveEducation(Education education) {
        if (education.getId() == 0) {
            personalManagmentRepository.addEducation(education);
        } else {
            personalManagmentRepository.updateEducation(education);
        }
    }
}
