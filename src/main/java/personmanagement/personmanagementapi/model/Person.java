package personmanagement.personmanagementapi.model;

import java.time.LocalDate;
import java.util.List;

public class Person {
    private int id;
    private String firstName;
    private String lastName;
    private LocalDate dateOfBirth;
    private List<Education> educations;
    private int educationTypeId;
    private String profilePhotoUrl;

    public Person(int id, String firstName, String lastName, LocalDate dateOfBirth, List<Education> educations, int educationTypeId, String profilePhotoUrl) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
        this.educations = educations;
        this.educationTypeId = educationTypeId;
        this.profilePhotoUrl = profilePhotoUrl;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public List<Education> getEducations() {
        return educations;
    }

    public void setEducations(List<Education> educations) {
        this.educations = educations;
    }

    public int getEducationTypeId() {
        return educationTypeId;
    }

    public void setEducationTypeId(int educationTypeId) {
        this.educationTypeId = educationTypeId;
    }

    public String getProfilePhotoUrl() {
        return profilePhotoUrl;
    }

    public void setProfilePhotoUrl(String profilePhotoUrl) {
        this.profilePhotoUrl = profilePhotoUrl;
    }
}
