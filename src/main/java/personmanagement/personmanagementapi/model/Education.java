package personmanagement.personmanagementapi.model;

public class Education {
    private int id;
    private String school;
    private int graduationYear;
    private int personId;

    public Education(int id, String school, int graduationYear, int personId) {
        this.id = id;
        this.school = school;
        this.graduationYear = graduationYear;
        this.personId = personId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public int getGraduationYear() {
        return graduationYear;
    }

    public void setGraduationYear(int graduationYear) {
        this.graduationYear = graduationYear;
    }

    public int getPersonId() {
        return personId;
    }

    public void setPersonId(int personId) {
        this.personId = personId;
    }
}
