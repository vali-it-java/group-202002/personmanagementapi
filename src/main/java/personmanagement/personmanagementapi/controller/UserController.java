package personmanagement.personmanagementapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import personmanagement.personmanagementapi.dto.*;
import personmanagement.personmanagementapi.service.UserService;

@RestController
@RequestMapping("/users")
@CrossOrigin("*")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/register")
    public GenericResponseDto register(@RequestBody UserRegistrationDto userRegistration) {
        return userService.register(userRegistration);
    }

    @PostMapping("/login")
    public JwtResponseDto authenticate(@RequestBody JwtRequestDto request) throws Exception {
        return userService.authenticate(request);
    }

    @PostMapping("/edituser")
    public void editUser(@RequestBody UserEditDto userEdit) {
        userService.editUser(userEdit);
    }
}

