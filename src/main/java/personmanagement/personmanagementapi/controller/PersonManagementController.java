package personmanagement.personmanagementapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import personmanagement.personmanagementapi.model.Education;
import personmanagement.personmanagementapi.model.EducationType;
import personmanagement.personmanagementapi.model.Person;
import personmanagement.personmanagementapi.repository.PersonalManagmentRepository;
import personmanagement.personmanagementapi.service.PersonManagementService;
import personmanagement.personmanagementapi.service.UserService;

import java.util.List;

@RestController
@RequestMapping("/persons")
@CrossOrigin("*")
public class PersonManagementController {

    @Autowired
    private PersonalManagmentRepository personalManagmentRepository;

    @Autowired
    private PersonManagementService personManagementService;

    @Autowired
    private UserService userService;

    @GetMapping("/all")
    public List<Person> getAllPersons() {
        System.out.println(userService.getCurrentlyLoggedInUser());
        return personalManagmentRepository.fetchPersons();
    }

    @PostMapping("/edit")
    public void postPerson(@RequestBody Person person) {
        personManagementService.savePerson(person);
    }

    @GetMapping("/educationTypes")
    public List<EducationType> getEducationTypes() {
        return personalManagmentRepository.getEducationTypes();
    }

    @PostMapping("/educations/edit")
    public void postEducation(@RequestBody Education education) {
        personManagementService.saveEducation(education);
    }

}
