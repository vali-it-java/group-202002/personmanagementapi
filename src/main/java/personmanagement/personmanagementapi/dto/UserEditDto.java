package personmanagement.personmanagementapi.dto;

public class UserEditDto {
    private String password;

    public UserEditDto() {

    }

    public UserEditDto(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
